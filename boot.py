# This file is executed on every boot (including wake-boot from deepsleep)
#import esp
#esp.osdebug(None)
import gc
#import webrepl
#webrepl.start()
gc.collect()

from utime import sleep
from utils.utils import blink
from utils.wifi import ap_shutdown

# make sure Wifi AP mode is shutdown
ap_shutdown()
# blink and wait 1 second for Wifi hw to be ready
blink()
sleep(1)

