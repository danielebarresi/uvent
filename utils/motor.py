from machine import Pin, PWM

class Motor:
    """
    PWM controlled DC motor driver
    """
    def __init__(self, pin):
        self.pwm = PWM(Pin(pin), freq=50, duty=0)
    def speed(self, speed):
        if speed > 1:
            duty = 1023
        else:
            duty = int(speed*1023)
        self.pwm.duty(duty)
