# This file is executed after boot.py

from utime import sleep
from machine import I2C, Pin, Timer, unique_id
from utils.utils import blink
from utils.wifi import Wifi
from utils.dht import get_sensor
from utils.motor import Motor
from umqtt.simple import MQTTClient
from ubinascii import hexlify
import json
import micropython

micropython.alloc_emergency_exception_buf(100)

print("Just woke up...")

DEV_ID = hexlify(unique_id()).decode()
print("Device ID: {}".format(DEV_ID))

#
# Hardware Pins and config
#

I2C_SCL = 5
I2C_SDA = 4
I2C_FREQ = 100000

MOTOR1_PWM = 0

#
# Global variables
#

# Timer ticks count
tick = 0
prev_tick = -1

# MQTT
mqtt_client = "uVent-{}".format(DEV_ID)
# DHT state topic
mqtt_dht_topic = "homeassistant/sensor/{}_DHT/state".format(DEV_ID).encode()
# Fan state (ON, OFF)
mqtt_fan_state_topic = "homeassistant/fan/{}/on/state".format(DEV_ID).encode()
# Fan command (ON, OFF)
mqtt_fan_topic = "homeassistant/fan/{}/on/set".format(DEV_ID).encode()
# Fan speed state (off, low, medium, high)
mqtt_fan_speed_state_topic = "homeassistant/fan/{}/speed/state".format(DEV_ID).encode()
# Fan speed command (off, low, medium, high)
mqtt_fan_speed_topic = "homeassistant/fan/{}/speed/set".format(DEV_ID).encode()

# MQTT topic subscription list
mqtt_sub_topics = [
    mqtt_fan_topic,
    mqtt_fan_speed_topic
    ]
# Buffer for incoming messages
mqtt_msg = {topic:[] for topic in mqtt_sub_topics}
mqtt_connected = False

# DHT sensor values
dht_temperature = 0
dht_humidity = 0
dht_update = []

# Temperature threshold
TEMP_TH_ON = 25
TEMP_TH_OFF = 22

# Operation mode: 0=auto; 1=manual
mode = 0

# Motor speed value
motor_speed = 0

#
# Functions
#

def wifi_connect(wifi):
    """
    Try to get a WiFi connection
    """
    with open('wifi.txt','r') as wifi_data:
        ssid = wifi_data.readline().rstrip()
        password = wifi_data.readline().rstrip()
        if wifi.within_range(ssid):
            # return True if WiFi connected
       	    return wifi.connect(ssid, password)
        return False

def mqtt_callback(topic, msg):
    """
    Handle mqtt incoming messages
    """
    print(topic, msg)
    if topic in mqtt_msg.keys():
        mqtt_msg[topic].append(msg)
    
def mqtt_init(mqtt_client, mqtt_callback):
    """
    Create mqtt object
    """
    with open('mqtt.txt','r') as mqtt_data:
        mqtt_broker = mqtt_data.readline().rstrip()
	mqtt_user = mqtt_data.readline().rstrip()
	mqtt_pwd = mqtt_data.readline().rstrip()
    mqtt = MQTTClient(mqtt_client, mqtt_broker, user=mqtt_user, password=mqtt_pwd)
    mqtt.set_callback(mqtt_callback)
    return mqtt

def mqtt_connect(mqtt):
    """
    Connect to MQTT broker and subscribe
    """
    try:
        mqtt.connect()
        return True
    except:
        return False

def mqtt_subscribe(mqtt, mqtt_sub_topics):
    """
    Subscribe to MQTT topics
    """
    try:
        for topic in mqtt_sub_topics:
            mqtt.subscribe(topic)
        return True
    except:
        return False

def mqtt_autodiscovery(mqtt, deviceId):
    """
    Publish the HASS.io autodiscovery message
    """
    mqtt_device=dict(identifiers=deviceId,name="uVent",manufacturer="BB Factory",model="Uno",sw_version="0.1")
    topics_list = [
	# DHT sensor
	("homeassistant/sensor/{}T/config".format(deviceId),
	dict(device_class="temperature",
	    name="Temperature",
            device=mqtt_device,
            unique_id="{}_temperature".format(deviceId),
	    state_topic="homeassistant/sensor/{}_DHT/state".format(deviceId),
	    unit_of_measurement= " C", # b'\xc2\xb0C',
	    value_template="{{ value_json.temperature }}")
	),
	("homeassistant/sensor/{}H/config".format(deviceId),
	dict(device_class="humidity",
	    name="Humidity",
            device=mqtt_device,
            unique_id="{}_humidity".format(deviceId),
	    state_topic="homeassistant/sensor/{}_DHT/state".format(deviceId),
	    unit_of_measurement="%",
	    value_template="{{ value_json.humidity }}")
	),
        # Fan
	("homeassistant/fan/{}F/config".format(deviceId),
	dict(name="Air flow",
            device=mqtt_device,
            unique_id="{}_fan".format(deviceId),
	    state_topic="homeassistant/fan/{}/on/state".format(deviceId),
	    command_topic="homeassistant/fan/{}/on/set".format(deviceId),
	    speed_state_topic="homeassistant/fan/{}/speed/state".format(deviceId),
	    speed_command_topic="homeassistant/fan/{}/speed/set".format(deviceId),
            speeds=['off','low','medium','high'])
	)
    ]
    try:
        for topic,payload in topics_list:
            mqtt.publish(topic.encode(), json.dumps(payload))
        return True
    except:
        return False

def timer1_callback(timer):
    global tick
    tick += 1

#
# Initial setup
#

print("Initializing...")

# Initialize I2C bus
i2c = I2C(scl=Pin(I2C_SCL), sda=Pin(I2C_SDA), freq=I2C_FREQ)

# Get DHT sensor
dht_sensor = get_sensor(i2c)

# Get motors
motor1 = Motor(MOTOR1_PWM)

# Set timer for periodic routines (every 10000 milliseconds)
timer1 = Timer(-1)
timer1.init(period=10000, mode=Timer.PERIODIC, callback=timer1_callback)

# Initialize WiFi (non blocking)
wifi = Wifi(timeout=-1)

# Initialize MQTT
mqtt = mqtt_init(mqtt_client, mqtt_callback)

# three short blinks to signal main loop will begin
blink(3)


#
# Main loop
#

print("Starting main loop...")
while True:
    # Every 10 seconds
    if tick > prev_tick:
        print("tick")

        # Update DHT sensor
        dht_sensor.measure()

        # Save DHT sensor readings
        dht_temperature = dht_sensor.temperature()
        dht_humidity = dht_sensor.humidity()
        dht_update.append(dict(temperature=dht_temperature,humidity=dht_humidity))

        # Check WiFi connection
        if not wifi.isconnected():
            print("Trying to connect to WiFi...")
	    wifi_connect(wifi)
        else:
            blink()

        # Check MQTT connection
        if wifi.isconnected() and not mqtt_connected:
            print("Connecting to MQTT broker...")
            if mqtt_connect(mqtt) and mqtt_subscribe(mqtt, mqtt_sub_topics):
                mqtt_connected = True
                print("Sending autodiscovery message...")
                if mqtt_autodiscovery(mqtt,DEV_ID):
                    print("Autodiscovery sent")
                else:
                    print("Autodiscovery failed")
            else:
                print("Error connecting to MQTT broker or subscribing")
        prev_tick = tick
 
    # Publish new DHT values
    if mqtt_connected and len(dht_update):
        try:
            mqtt.publish(mqtt_dht_topic, json.dumps(dht_update.pop()))
        except:
            mqtt_connected = False

    # Update Fan status (ON, OFF)
    if len(mqtt_msg[mqtt_fan_topic]):
        fan_cmd = mqtt_msg[mqtt_fan_topic].pop()
        fan_cmd = fan_cmd.decode()
        # make shure valid command was received
        if fan_cmd in ['ON', 'OFF']:
            mode = fan_cmd
            try:
                mqtt.publish(mqtt_fan_state_topic, mode.encode())
            except:
                mqtt_connected = False
    # Without MQTT connection fallback to ON mode
    if mode == 'OFF' and not mqtt_connected:
        mode = 'ON'

    # Update Fan speed
    if mode == 'ON':
        # In ON mode, set speed from MQTT
        if len(mqtt_msg[mqtt_fan_speed_topic]):
            fan_speed = mqtt_msg[mqtt_fan_speed_topic].pop()
            fan_speed = fan_speed.decode()
            if fan_speed == 'off':
                motor_speed = 0
            elif fan_speed == 'low':
                motor_speed = 0.5
            elif fan_speed == 'medium':
                motor_speed = 0.75
            elif fan_speed == 'high':
                motor_speed = 1
            motor1.speed(motor_speed)
            try:
                mqtt.publish(mqtt_fan_speed_state_topic, fan_speed.encode())
            except:
                mqtt_connected = False
    else:
        # In OFF mode, set speed based on temperature
        if motor_speed == 0 and dht_temperature > TEMP_TH_ON:
            # Above high threshold, start motor
            motor_speed = 1
            fan_speed = 'high'
        elif motor_speed > 0 and dht_temperature > TEMP_TH_OFF:
            # Still above low threshold, keep motor running
            motor_speed = 1
            fan_speed = 'high'
        else:
            # Below threshold, stop motor
            motor_speed = 0
            fan_speed = 'off'
        motor1.speed(motor_speed)
        try:
            mqtt.publish(mqtt_fan_speed_state_topic, fan_speed.encode())
        except:
            mqtt_connected = False

    # Check new MQTT messages
    if mqtt_connected:
        try:
            mqtt.check_msg()
        except:
            mqtt_connected = False
