# MicroPython on Wemos D1 mini PRO

## System specs

This project is based on a Wemos D1 mini PRO board, featuring an esp8266 microcontroller, running microPython.

You can find microPython documentation specific for this platform here: http://docs.micropython.org/en/latest/esp8266/

Currently the code is tested on microPython v1.9.1.

## Stuff you will need on your computer

Install `esptool` and `ampy`:
```
pip3 install esptool
pip3 install adafruit-ampy
```

You will also need to install the [CP210x USB to UART driver from Silicon Labs](https://www.silabs.com/products/development-tools/software/usb-to-uart-bridge-vcp-drivers). Choose the appropriate release for your platform (Mac, Linux, Windows).

## Useful commands

A collection of useful commands for managing microPython on a Wemos D1 mini PRO board.

Delete flash memory:
```
$ esptool.py --port /dev/tty.SLAB_USBtoUART erase_flash
```

Install microPython (for now supports up to 4MB flash memory):
```
$ esptool.py --port /dev/tty.SLAB_USBtoUART --baud 115200 write_flash --flash_size=4MB -fm=dio 0 esp8266-xxxxxxxx-vx.x.x.bin
```

Connect to the REPL:
```
$ screen /dev/tty.SLAB_USBtoUART 115200
```

Download boot.py file from the board:
```
$ ampy -p /dev/tty.SLAB_USBtoUART get boot.py > boot.py 
```

Upload boot.py to the board:
```
$ ampy -p /dev/tty.SLAB_USBtoUART put boot.py
```

## WiFi connection

The esp8266 microcontroller has WiFi support.
The code in boot.py expects you to create a wifi.txt file with SSID and password for the WiFi network. It will connect when booted.

## Additional drivers

To use the Wemos DHT shield v2.0.0 the DHT12 driver is needed (https://raw.githubusercontent.com/mcauser/micropython-dht12/master/dht12.py).