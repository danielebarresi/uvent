import network
from utime import sleep

def ap_shutdown():
    """Deactivate WiFi AP"""
    ap_if = network.WLAN(network.AP_IF)
    if ap_if.active() == True:
        ap_if.active(False)
    while ap_if.active() == True:
        pass

class Wifi:
    """WLAN object wrapper for easier Wifi handling in client mode.
    For some reason network.WLAN is not an actual class (as documented), 
    but seems to be a function that returns a WLAN object."""
    def __init__(self,timeout=5):
        self.wlan = network.WLAN(network.STA_IF)
        self.timeout = timeout

    def active(self,state):
        self.wlan.active(state)

    def isconnected(self):
        return self.wlan.isconnected()

    def connect(self, ssid, password):
        """Setup Wifi connection with local AP"""
        if not self.isconnected():
            self.active(True)
            self.wlan.connect(ssid, password)
            count = 0
            while not self.isconnected():
                if count > self.timeout:
                    return False
                count += 1
                sleep(1)  # wait 1 second before checking again
        return True

    def within_range(self, ssid):
        """Scan Wifi and return True if the given ssid if found"""
        available_networks = [bytes.decode(result[0]) for result in self.wlan.scan()]
        if ssid in available_networks:
            return True
        else:
            return False

