import dht12

def get_sensor(i2c):
    return dht12.DHT12(i2c)
