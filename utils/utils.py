from machine import Pin
from utime import sleep

def blink(times=1,seconds=0.25,duty_cycle=0.3):
    """Blik the embedded led n times"""
    pin2 = Pin(2, Pin.OUT)
    time_on = seconds * duty_cycle
    time_off = seconds * (1 - duty_cycle)
    while times:
        pin2.off()  # led on
        sleep(time_on)
        pin2.on()  # led off
        times -= 1
        if times:
            # wait time_off before next blink
            sleep(time_off)
